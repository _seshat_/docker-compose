# ubusrvk8s-master

*dockerfile*

# install npm and nodejs
sudo apt install npm nodejs


# install vue-cli
sudo npm install -g @vue/cli

# if bug last step
sudo chown -R $USER ~/.local/lib/node_modules

# generate vue project
vue create estiam101

# build and tag dockerfile
sudo docker build -t ubuvue .

# run dockerfile in daemon
sudo docker run ubuvue -d

# display running containers
sudo docker ps

# stop docker container
sudo docker stop <image>

# remove docker images
sudo docker rm <image>

# list docker images
sudo docker images

*docker-compose*

# launch compose services
sudo docker compose up

# launch docker compose daemon
sudo docker compose up -d

# launch specific service
sudo docker compose up <service-name>

# launch specific service in daemon
sudo docker compose up <service-name> -d