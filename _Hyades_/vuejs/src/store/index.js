import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import axios from 'axios'

export default new Vuex.Store({
  state: {
    cli:{
      ip:null,
      ua:null,
    },
    win: {
      W: 0,
      H: 0,
    },
    grids: {},
    __lib__: {
      yaml: require('js-yaml')
    },
  },
  mutations: {
    // GRIDS
    getGrids: (state, cnf) => {
      axios.get(`https://api.aendlyx.tech/grids/${cnf.id}`)
      .then(rsp=>{         

        // log JSON rsp.data
        console.log(rsp.data)

        // :id="cssid"
        state.grids[cnf.name] = {cssid: rsp.data.cssid,style: {}, elements: []}
        console.log(state.grids[cnf.name].cssid)

        // :style="style"
        state.__lib__.yaml.safeLoadAll(rsp.data.style, (doc) => {
          Object.assign(state.grids[cnf.name]['style'], doc)
        })

        // iterates through elements
        rsp.data.grid_elements.forEach(gridElement => {
          // init gridElementObject
          var gElm = {cssid: "", style: {}, data: ""}
          console.log(gridElement)
          for (let [key, value] of Object.entries(gridElement)) {
            // only target thoses keys
            if (key === "data" || key === "style" 
            ||  key === "cssid" || key === "domtype" ){
              // only yaml element
              if (key === "style") {
                state.__lib__.yaml.safeLoadAll(value, (doc) => {
                  Object.assign(gElm[key], doc)
                })
              // simple variable
              } else {
                gElm[key]=value
              }
            }
          }
          console.log(`<*- gElm -*>`)
          console.log(gElm)
          state.grids[cnf.name]['elements'].push(gElm)
        })
      })
      .catch(err=>{ console.log(err) })
      .then(rsp=>{
        console.log(`<*- / gElm \ -*>`)
      })
    },
    // TRACKERS
    track: (state) => {
      state.cli.ua = navigator.userAgent
      axios.get('https://jsonip.com/')
      .then(rsp=>{
        let json = rsp.data
        state.cli.ip = json.ip
        axios.post('https://api.aendlyx.tech/trackers', {
          pj: "beowulf_asia",
          ua: state.cli.ua,
          ip: state.cli.ip
        })
        .then(rsp=>{
          // console.log(rsp.data)
        })
        .catch(err=>{console.log(err)})
      })
      .catch(err=>{console.log(err)})
    },
    // WINDOW ---------------------------------------------
    updTitle: (state, title) => {
      document.title = title
    },
  },
  actions: {
  },
  modules: {
  },
  getters: {
    getGrid: (state) => (name) => {
      console.log(`$store.getters.getGrid(${name})`)
      return state.grids[name]
    }
  }
})
