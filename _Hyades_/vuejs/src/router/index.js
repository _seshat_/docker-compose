import Vue from 'vue'
import VueRouter from 'vue-router'

import Grids from '../views/Grids.vue'
import BeowulfAsia from '../views/BeowulfAsia.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'BeowulfAsia',
    component: BeowulfAsia
  },

  {
    path: '/grids',
    name: 'grids',
    component: Grids
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
