
const __website__ = {
  "project":process.env.Project,
  "description":process.env.Description
}

module.exports = {
  devServer: {
    proxy: 'http://localhost:8080',
    public: '51.15.103.216:8080',
    before: function(app, server) {
      app.get('/__website__', function(req, res) {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end(JSON.stringify(__website__))
      })
    },
  }
}