https://gitlab.com/_seshat_/docker-compose

# Demarrer tous les services du docker-compose
> sudo docker-compose up
*La console affiche les logs des differentes services*

# Demarrer un service du docker-compose
> sudo docker-compose up <service>
> sudo docker-compose up postgres

# Demarrer les services du docker-compose en daemon
> sudo docker-compose up -d
*Les services sont lances en arriere plan*

# Demarrer les services du docker-compose et re-build les dockerfiles
> sudo docker-compose up -d --build
*Les services sont lances en arriere plan et les dockerfile reconstruit*

# Arreter les services du docker-compose
> sudo docker-compose down

# Arreter les services du docker-compose et supprimer les volumes
> sudo docker-compose down --volumes
